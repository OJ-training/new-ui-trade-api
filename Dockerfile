## NOTE: to use this for your angular app you should change
##       the COPY line below. Replace "ShippersApp" with your
##       angular app name. You'll find this in angular.json as the
##       projectName.

# Stage 1: Compile and Build angular codebase

# Use official node image as the base image
FROM node:14.15.1 as build

# Set the working directory
WORKDIR /home/grads/new-ui-trade-api/


# Add the source code to app
COPY ./ /home/grads/new-ui-trade-api/

# Install all the dependencies
RUN npm install

# Generate the build of the application
RUN npm run build

# For logging print the name of the generated dir
RUN ls /home/grads/new-ui-trade-api/dist/


# Stage 2: Serve app with nginx server

# Use nginx unprivileged (non-root) image as the base image
FROM nginxinc/nginx-unprivileged

# Copy the build output to replace the default nginx contents.
COPY --from=build /home/grads/new-ui-trade-api/dist/UI-trade-api/ /usr/share/nginx/html

# Expose port 80
EXPOSE 8080