import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTradingHistoryComponent } from './view-trading-history.component';

describe('ViewTradingHistoryComponent', () => {
  let component: ViewTradingHistoryComponent;
  let fixture: ComponentFixture<ViewTradingHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewTradingHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTradingHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
