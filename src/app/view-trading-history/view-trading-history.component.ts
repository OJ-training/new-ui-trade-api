import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service";
import { Stock } from '../shared/stock';

@Component({
  selector: 'app-view-trading-history',
  templateUrl: './view-trading-history.component.html',
  styleUrls: ['./view-trading-history.component.css']
})
export class ViewTradingHistoryComponent implements OnInit {

  Stocks: any = [];
  constructor( 
    public restApi: RestApiService
    ) { }

  ngOnInit(): void {
    this.loadStocks()
  }

  loadStocks() {
    return this.restApi.getTradingHistory().subscribe((data: {}) => {
        this.Stocks = data;
        
    })
  } 

}
