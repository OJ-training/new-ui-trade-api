import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service";

@Component({
  selector: 'app-view-portfolio',
  templateUrl: './view-portfolio.component.html',
  styleUrls: ['./view-portfolio.component.css']
})
export class ViewPortfolioComponent implements OnInit {

  Stocks: any = [];
  Statement: any=[];
  constructor( 
    public restApi: RestApiService
    ) { }

  ngOnInit(): void {
    this.loadStocks()
    this.loadStatement()
  }

  loadStocks() {
    return this.restApi.getPortfolio().subscribe((data: {}) => {
        this.Stocks = data;
    })
  }
  
  loadStatement() {
    return this.restApi.getStatement().subscribe((data: {}) => {
        this.Statement = data;
    })
  }

}
