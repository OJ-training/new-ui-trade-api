import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TradeStocksComponent } from './trade-stocks/trade-stocks.component';
import { ViewPortfolioComponent } from './view-portfolio/view-portfolio.component';
import { ViewTradingHistoryComponent } from './view-trading-history/view-trading-history.component';
import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './about-us/about-us.component';



@NgModule({
  declarations: [
    AppComponent,
    TradeStocksComponent,
    ViewPortfolioComponent,
    ViewTradingHistoryComponent,
    HomeComponent,
    AboutUsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
