import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Stock } from '../shared/stock';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { TradeStocksComponent } from '../trade-stocks/trade-stocks.component';


@Injectable({
  providedIn: 'root'
})
export class RestApiService {
  apiURL = 'http://trading-demo-trading-demo.punedevopsa58.conygre.com/api/stocks/';
  public ticker="";

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  

  // HttpClient API post() method 
  createStock(stock:Stock): Observable<Stock> {
    return this.http.post<Stock>(this.apiURL + '', JSON.stringify(stock), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  } 

  getPortfolio(): Observable<Stock>{
    return this.http.get<Stock>(this.apiURL + "portfolio")
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getTradingHistory(): Observable<Stock>{
    return this.http.get<Stock>(this.apiURL + "history")
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getStatement(): Observable<Stock>{
    return this.http.get<Stock>(this.apiURL + "statement")
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

getSelectedTicker(tic:any){
  this.ticker= tic;
  console.log("Selected ticker"+tic);
}

  getLivePrice(): Observable<number>{
    return this.http.get<number>(this.apiURL + "livePrice/"+this.ticker)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }


  // Error handling 
  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }



  // // HttpClient API get() method 
  // getShipper(id:any): Observable<Shipper> {
  //   return this.http.get<Shipper>(this.apiURL + id)
  //   .pipe(
  //     retry(1),
  //     catchError(this.handleError)
  //   )
  // }  

  // // HttpClient API post() method 
  // createShipper(shipper:Shipper): Observable<Shipper> {
  //   return this.http.post<Shipper>(this.apiURL + '', JSON.stringify(shipper), this.httpOptions)
  //   .pipe(
  //     retry(1),
  //     catchError(this.handleError)
  //   )
  // } 

  // // HttpClient API put() method
  // updateShipper(id:number, shipper:Shipper): Observable<Shipper> {
  //   return this.http.put<Shipper>(this.apiURL, JSON.stringify(shipper), this.httpOptions)
  //   .pipe(
  //     retry(1),
  //     catchError(this.handleError)
  //   )
  // }

  // // HttpClient API delete() method 
  // deleteShipper(id:number){
  //   return this.http.delete<Shipper>(this.apiURL + id, this.httpOptions)
  //   .pipe(
  //     retry(1),
  //     catchError(this.handleError)
  //   )
  // }

  
  
}
