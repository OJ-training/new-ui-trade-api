export class Stock {
    constructor(){
        this.id=0;
        this.date="";
        this.stockTicker="";
        this.price=0;
        this.quantity=0;
        this.buyOrSell="";
        this.statusCode=0;
    }
    id: number;
    date: string;
    stockTicker: string;
    price: number;
    quantity: number;
    buyOrSell: string;
    statusCode: number;
}