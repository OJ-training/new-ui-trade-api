import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutUsComponent } from './about-us/about-us.component';
import { HomeComponent } from './home/home.component';
import {TradeStocksComponent} from './trade-stocks/trade-stocks.component';
import {ViewPortfolioComponent} from './view-portfolio/view-portfolio.component';
import {ViewTradingHistoryComponent} from './view-trading-history/view-trading-history.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: 'home', component: HomeComponent },
  { path: 'trade-stocks', component: TradeStocksComponent },
  { path: 'view-portfolio', component: ViewPortfolioComponent },
  { path: 'view-trading-history', component: ViewTradingHistoryComponent },
  { path: 'about-us', component: AboutUsComponent }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
