import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TradeStocksComponent } from './trade-stocks.component';

describe('TradeStocksComponent', () => {
  let component: TradeStocksComponent;
  let fixture: ComponentFixture<TradeStocksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TradeStocksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TradeStocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
