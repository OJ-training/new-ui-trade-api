import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service";
import { DatePipe } from '@angular/common';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-trade-stocks',
  templateUrl: './trade-stocks.component.html',
  styleUrls: ['./trade-stocks.component.css']
})
export class TradeStocksComponent implements OnInit {

  myDate = new Date()
  dd = String(this.myDate.getDate()).padStart(2, '0');
  mm = String(this.myDate.getMonth() + 1).padStart(2, '0'); //January is 0!
  yyyy = this.myDate.getFullYear();
  
  today =  this.yyyy + '-'+ this.mm + '-' + this.dd;

  @Input() stockDetailsBuy = { id:0, date:this.today, stockTicker: '', price:0, quantity:0, buyOrSell: '', statusCode:0 }
  @Input() stockDetailsSell = { id:0, date:this.today, stockTicker: '', price:0, quantity:0, buyOrSell: '', statusCode:0 }


  constructor(
    public restApi: RestApiService, 
    public router: Router,
  ) {}

  livePriceBuy:number=0;
  livePriceSell:number=0;

  myControl = new FormControl();
  options: string[] = ['One', 'Two', 'Three'];
  filteredOptions!: Observable<string[]>;

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }



  addStockBuy() {
    console.log("in addstockbuy of tradestock")
    this.stockDetailsBuy.stockTicker = this.GetSelectedValueBuy();
    this.stockDetailsBuy.buyOrSell = "Buy";
    this.restApi.createStock(this.stockDetailsBuy).subscribe((data: {}) => {
      this.router.navigate(['/view-trading-history'])
    })
  }

  loadPriceBuy(){
    console.log("inside loadPrice of trade-stocks component");
    this.sendSelectedTickerBuy();
    return this.restApi.getLivePrice().subscribe((data:number) => {
      this.livePriceBuy = data;
  })
}

  loadPriceSell(){
    console.log("inside loadPrice of trade-stocks component");
    this.sendSelectedTickerSell();
    return this.restApi.getLivePrice().subscribe((data:number) => {
      this.livePriceSell = data;
  })
  }

  sendSelectedTickerBuy(){
    var e = document.getElementById("tickersBuy") as HTMLSelectElement;
    var result = e.options[e.selectedIndex].value;
    this.restApi.getSelectedTicker(result);
  }

  sendSelectedTickerSell(){
    var e = document.getElementById("tickersSell") as HTMLSelectElement;
    var result = e.options[e.selectedIndex].value;
    this.restApi.getSelectedTicker(result);
  }

  addStockSell() {
    this.stockDetailsSell.stockTicker = this.GetSelectedValueSell();
    this.stockDetailsSell.buyOrSell = "Sell";
    this.restApi.createStock(this.stockDetailsSell).subscribe((data: {}) => {
      this.router.navigate(['/view-trading-history'])
    })
  }

  GetSelectedValueBuy(){
    var e = document.getElementById("tickersBuy") as HTMLSelectElement;
    var result = e.options[e.selectedIndex].value;
    return result;
}

  GetSelectedValueSell(){
    var e = document.getElementById("tickersSell") as HTMLSelectElement;
    var result = e.options[e.selectedIndex].value;
    return result;
}

  



}
